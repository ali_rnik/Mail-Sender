#include <vector>
#include <string>
#include <thread>
#include <sys/inotify.h>
#include <limits.h>
#include <map>
#include <mutex>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sstream>
#include <fstream>

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

#define WEBUI_SIGNAL "/tmp/webui_signal"
#define SYSTEM_SIGNAL "/tmp/system_signal"
#define SYSTEM_DATA "/tmp/system_data"

#define HOST "localhost"
#define USER "webui"
#define PASSWORD "mysql"

#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

static std::vector<std::string> mail_list;
static std::string sender_mail;
static std::mutex mtx;

static int webui_signal_handler();
static int system_signal_handler();
static int sendmail(std::string subject, std::string body);
static int init();

using namespace std;

int main()
{
	mail_list.clear();

	fprintf(stdout, "%d: %s: Program Started!\n\n", __LINE__, __func__);

	init();

	std::thread webui_thread(webui_signal_handler);
	std::thread system_thread(system_signal_handler);

	webui_thread.join();
	system_thread.join();
}

static int system_signal_handler()
{
	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	int num_read, system_signal_fd;
	std::ifstream inputstream;
	std::string subject, body, stage;

	system_signal_fd = inotify_init();
	inotify_add_watch(system_signal_fd, SYSTEM_SIGNAL, IN_MODIFY);

	while (1) {
		subject.clear(), body.clear();

		fprintf(stdout, "%d: %s: Waiting for system signal!\n",
			__LINE__, __func__);

		num_read = read(system_signal_fd, buf, BUF_LEN);

		mtx.lock();

		if (num_read == 0)
			fprintf(stdout, "%d: %s: Strange thing happend and \
				read returned 0!\n", __LINE__, __func__);
		if (num_read == -1)
			fprintf(stdout, "%d: %s: %s\n", __LINE__, __func__,
				strerror(errno));

		fprintf(stdout, "%d: %s: system signal catched!\n\n",
			__LINE__, __func__);

		inputstream.open(SYSTEM_DATA);

		while (inputstream >> stage) {
			if (stage == "&&&&")
				break;
			else
				subject += stage + " ";
		}
		subject.pop_back();

		while (inputstream >> stage)
				body += stage + " ";
		body.pop_back();

		sendmail(subject, body);

		inputstream.close();
		mtx.unlock();
	}
}

static int webui_signal_handler()
{
	int num_read, webui_signal_fd;
	char buf[BUF_LEN] __attribute__ ((aligned(8)));
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
	string root, AuthUser, AuthPass, FromLineOverride, UseTLS,
		rewriteDomain, mailhub;
	ofstream outfile;

	UseTLS = "UseTLS=YES";
	FromLineOverride = "FromLineOverride=YES";

	webui_signal_fd = inotify_init();
	inotify_add_watch(webui_signal_fd, WEBUI_SIGNAL, IN_MODIFY);

	driver = get_driver_instance();
	con = driver->connect(HOST, USER, PASSWORD);
	stmt = con->createStatement();

	stmt->execute((std::string) "USE " + (std::string) "webui_sendmail");
	while (1) {
		fprintf(stdout, "%d: %s: Waiting for webui signal!\n",
			__LINE__, __func__);

		num_read = read(webui_signal_fd, buf, BUF_LEN);

		mtx.lock();

		if (num_read == 0)
			fprintf(stdout, "%d: %s: Strange thing happend and \
				read returned 0!\n",__LINE__, __func__);
		if (num_read == -1)
			fprintf(stdout, "%d: %s: %s\n", __LINE__, __func__,
				strerror(errno));

		fprintf(stdout, "%d: %s: webui signal catched!\n\n",
			__LINE__, __func__);

		fprintf(stdout, "%d: %s: Trying to update lists from DB !\n",
			__LINE__, __func__);

		mail_list.clear();
		res = stmt->executeQuery("SELECT si_users.email FROM \
			`si_users`  INNER JOIN si_alarm \
			ON(si_alarm.`user_id`=si_users.id)");

		while (res->next())
			mail_list.push_back(res->getString("email"));

		res = stmt->executeQuery("SELECT * FROM si_alarm_config WHERE \
			param='email'");

		while (res->next())
			root = "root=" + res->getString("value");

		res = stmt->executeQuery("SELECT * FROM si_alarm_config WHERE \
			param='password'");

		while (res->next())
			AuthPass = "AuthPass=" + res->getString("value");

		AuthUser = "AuthUser=" + root.substr(5);

		sender_mail = root.substr(5);

		rewriteDomain = "rewriteDomain="
				+ root.substr(root.find('@')+1);
		mailhub = "mailhub=smtp." + rewriteDomain.substr(14) + ":465";

		outfile.open("/etc/ssmtp/ssmtp.conf",
			std::ofstream::out | std::ofstream::trunc);

		outfile << root << endl << mailhub << endl << rewriteDomain
			<< endl << AuthPass << endl << AuthUser << endl 
			<< UseTLS << endl << FromLineOverride << endl;

		outfile.close();

		outfile.open("/etc/ssmtp/revaliases",
			std::ofstream::out | std::ofstream::trunc);

		outfile << "root:" << root.substr(5) << endl;

		outfile.close();

		fprintf(stdout, "%d: %s: lists updated from DB !\n\n",
			__LINE__, __func__);

		mtx.unlock();
	}
}

static int sendmail(std::string subject, std::string body)
{
	std::string command;
	std::ofstream outfile;

	for (int i = 0; i < (int) mail_list.size(); i++) {
		fprintf(stdout, "%d: %s: Trying to send email for : %s\n",
			__LINE__, __func__, mail_list[i].data());

		outfile.open("/tmp/mailtext",
			std::ofstream::out | std::ofstream::trunc);

		outfile << "To:" << mail_list[i] << endl;
		outfile << "From:" << sender_mail << endl;
		outfile << "Subject :" << subject << endl << endl;
		outfile << body << endl;

		outfile.close();

		command = "/usr/sbin/ssmtp -tv < /tmp/mailtext";
		system(command.data());

		fprintf(stdout, "%d: %s: email sent for %s with the following \
			content: \n\n%s\n%s\n\n" , __LINE__, __func__,
			mail_list[i].data(), subject.data(), body.data());
	}

	return 0;
}

static int init()
{
	if (creat(WEBUI_SIGNAL, 0777) == -1) {
		fprintf(stdout, "%d: %s: %s\n", __LINE__, __func__,
			strerror(errno));
		return -1;
	}

	if (creat(SYSTEM_DATA, 0777) == -1) {
		fprintf(stdout, "%d: %s: %s\n", __LINE__, __func__,
			strerror(errno));
		return -1;
	}

	if (creat(SYSTEM_SIGNAL, 0777) == -1) {
		fprintf(stdout, "%d: %s: %s\n", __LINE__, __func__,
			strerror(errno));
		return -1;
	}

	return 0;
}
