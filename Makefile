compile-and-link: main.o
	g++ main.o -o Mail-Sender -lpthread -lmysqlcppconn
	rm -rf main.o

main.o:
	g++ -c main.cpp -o main.o

clean:
	rm -rf Mail-Sender main.o
