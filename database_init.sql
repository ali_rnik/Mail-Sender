create database webui_sendmail;

use webui_sendmail;


create table receivers
(
	id int,
	email varchar(255),
);

create table events
(
	id int,
	event varchar(255),
);

GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER on receivers to 'strong'@'localhost' identified by 'strong';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER on events to 'strong'@'localhost' identified by 'strong';

